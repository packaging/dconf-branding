#!/bin/bash

create_package() {
local TYPE="$1"
local FOLDERS=$(find $THEME_DIR -mindepth 1 -maxdepth 1 -type d -printf '%f\n')
fpm -f -s dir -t $TYPE -n "${THEME_DIR#*/}-branding" \
--prefix="/" \
--vendor "${VENDOR:-Me}" \
--maintainer "${MAINTAINER:-Myself}" \
--version "$(date +%Y%m%d)~git~$(git rev-parse --short HEAD)" \
--description "meta package for various ${THEME_DIR#*/} branding settings" \
--after-install package_scripts/after-install.sh \
--before-remove package_scripts/before-remove.sh \
--depends lightdm-theme-${THEME_DIR#*/} \
--depends numix-cinnamon-theme \
--depends numix-icon-theme-circle \
--depends plymouth-theme-${THEME_DIR#*/} \
-C ${THEME_DIR} \
$(echo $FOLDERS)
}

which fpm > /dev/null

if [ $? -eq 0 ]; then
  if [ -d "$1" ]; then
    THEME_DIR="$1"
  else
    THEME_DIR="package_root/my-logo"
  fi
  create_package deb
else
  echo "fpm not found in PATH"
  exit 1
fi
