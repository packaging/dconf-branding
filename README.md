# Ubuntu dconf branding

It might happen that you'll need to deploy some sort of corporate identity to Ubuntu clients (e.g. in schools, at work, at home, just for fun,...). Therefore it might also be easy, to deploy a package, which takes care of things rather than doing all manually. This package can then be triggerd by some orchestration tools (e.g. Puppet, Salt, Ansible, Chef, whatever,...) or directly when installing via Preseed. So far, so good, here's a simple template which is utilizing ```dconf``` to set some theme-related settings for users like wallpaper, fonts,...

Right now, this is for Cinnamon Desktop! If you are using another desktop environment, check [this section](#desktop-environment)

Together with [Plymouth Logo template](https://gitlab.com/packaging/plymouth-logo-theme-template/) and [LightDM Logo template](https://gitlab.com/packaging/lightdm-logo-theme-template/), this will provide a consistent, branded look starting at boot time, during login and afterwards.

## Customization

Best thing is to create a new theme directory in ```package_root``` and adjust things there.

```
MY_CI_TARGET="acme-inc-logo"
cp -R package_root/my-logo package_root/$MY_CI_TARGET
rename 's/my-branding/'"$MY_CI_TARGET"'/' package_root/$MY_CI_TARGET/etc/dconf/db/local.d/*
```

### Wallpaper

This template just depends on the [LightDM logo template](https://gitlab.com/packaging/lightdm-logo-theme-template/) and it's wallpaper. If you want another wallpaper for user sessions, just create another directory inside ```package_root/$MY_CI_TARGET```, place it there and update ```package_root/$MY_CI_TARGET/etc/dconf/db/local.d/01-$MY_CI_TARGET``` to reflect the changes. Remember that the ```package_root``` directory equals ```/``` on your target system!

Example:

```
mkdir -p package_root/$MY_CI_TARGET/usr/share/backgrounds/$MY_CI_TARGET
wget http://trunkweed.com/uploads/posts/images/406303-coyote-acme-wallpaper.png -O package_root/$MY_CI_TARGET/usr/share/backgrounds/$MY_CI_TARGET/background.png
sed -i 's,/usr/share/backgrounds/my-logo/background.png,package_root/'"$MY_CI_TARGET"'/usr/share/backgrounds/'"$MY_CI_TARGET"'/background.png,' package_root/$MY_CI_TARGET/etc/dconf/db/local.d/01-$MY_CI_TARGET
```

### GTK theme, icon themes

Just have a look inside ```package_root/$MY_CI_TARGET/etc/dconf/db/local.d/01-$MY_CI_TARGET```

### Desktop environment

You can utilize ```dconf``` to navigate through the settings depending on your DE. Here's an example assuming your using Unity ( which in fact is just using GNOMEs settings):

Note: tab completion helps a lot ;)

```
dconf dump /org/gnome/desktop/interface
```

Change whatever value you like.

## Packaging

Just call the script ```create.sh``` with your theme directory as parameter. You should also set the variables ```VENDOR``` and ```MAINTAINER```. You'll need [ruby](http://rvm.io/rvm/install) and the fpm gem (```gem install fpm --no-ri --no-rdoc```). If you don't want to mess up your system, just use Docker:

```
docker run --rm -it -v /tmp/lightdm-logo-theme-template/:/build -w /build ruby:latest /bin/bash
apt-get update && apt-get install ruby ruby-dev -y
gem install fpm --no-ri --no-rdoc
VENDOR="ACME Inc." MAINTAINER="$VENDOR" ./create.sh "package_root/$MY_CI_TARGET"
```
